.. _release-9-11-1:

Version 9.12.1
==============

The significant changes to the various parts of the compiler are listed in the
following sections. See the `migration guide
<https://gitlab.haskell.org/ghc/ghc/-/wikis/migration/9.12>`_ on the GHC Wiki
for specific guidance on migrating programs to this release.

Language
~~~~~~~~


- The ordering of variables used for visible type application has been changed in two cases.
  It is supposed to be left-to-right, but due to an oversight, it was wrong:

  - in an infix application ``f :: a `op` b``, it is now ``forall a op b.`` rather than
    ``forall op a b.``
  - in a linear type ``f :: a %m -> b``, it is now ``forall a m b.`` rather than
    ``forall a b m.``.

  This change is backwards-incompatible, although in practice we don't expect it
  to cause significant disruption.

- The built-in ``HasField`` class, used by :extension:`OverloadedRecordDot`, now
  supports representation polymorphism (implementing part of `GHC Proposal #583
  <https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0583-hasfield-redesign.rst>`_).
  This means that code using :extension:`UnliftedDatatypes` or
  :extension:`UnliftedNewtypes` can now use :extension:`OverloadedRecordDot`.


Compiler
~~~~~~~~

- Constructor ``PluginProv`` of type ``UnivCoProvenance``, relevant
for typing plugins, gets an extra ``DCoVarSet`` argument.
The argument is intended to contain the in-scope coercion variables
that the the proof represented by the coercion makes use of.
See ``Note [The importance of tracking free coercion variables]``
in ``GHC.Core.TyCo.Rep``, :ref:`constraint-solving-with-plugins`
and the migration guide.

- The flag `-fprof-late` will no longer prevent top level constructors from being statically allocated.

  It used to be the case that we would add a cost centre for bindings like `foo = Just bar`.
  This turned the binding into a caf that would allocate the constructor on first evaluation.

  However without the cost centre `foo` can be allocated at compile time. This reduces code-bloat and
  reduces overhead for short-running applications.

  The tradeoff is that calling `whoCreated` on top level value definitions like `foo` will be less informative.

- A new flag ``-fexpose-overloaded-unfoldings`` has been added providing a lightweight alternative to ``-fexpose-all-unfoldings``.

GHCi
~~~~


Runtime system
~~~~~~~~~~~~~~

- Reduce fragmentation incurred by the nonmoving GC's segment allocator. In one application this reduced resident set size by 26%. See :ghc-ticket:`24150`.

``base`` library
~~~~~~~~~~~~~~~~


``ghc-prim`` library
~~~~~~~~~~~~~~~~~~~~

``ghc`` library
~~~~~~~~~~~~~~~

``ghc-heap`` library
~~~~~~~~~~~~~~~~~~~~

``ghc-experimental`` library
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``template-haskell`` library
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Included libraries
~~~~~~~~~~~~~~~~~~

The package database provided with this distribution also contains a number of
packages other than GHC itself. See the changelogs provided with these packages
for further change information.

.. ghc-package-list::

    libraries/array/array.cabal:             Dependency of ``ghc`` library
    libraries/base/base.cabal:               Core library
    libraries/binary/binary.cabal:           Dependency of ``ghc`` library
    libraries/bytestring/bytestring.cabal:   Dependency of ``ghc`` library
    libraries/Cabal/Cabal/Cabal.cabal:       Dependency of ``ghc-pkg`` utility
    libraries/Cabal/Cabal-syntax/Cabal-syntax.cabal:  Dependency of ``ghc-pkg`` utility
    libraries/containers/containers/containers.cabal: Dependency of ``ghc`` library
    libraries/deepseq/deepseq.cabal:         Dependency of ``ghc`` library
    libraries/directory/directory.cabal:     Dependency of ``ghc`` library
    libraries/exceptions/exceptions.cabal:   Dependency of ``ghc`` and ``haskeline`` library
    libraries/filepath/filepath.cabal:       Dependency of ``ghc`` library
    compiler/ghc.cabal:                      The compiler itself
    libraries/ghci/ghci.cabal:               The REPL interface
    libraries/ghc-boot/ghc-boot.cabal:       Internal compiler library
    libraries/ghc-boot-th/ghc-boot-th.cabal: Internal compiler library
    libraries/ghc-compact/ghc-compact.cabal: Core library
    libraries/ghc-heap/ghc-heap.cabal:       GHC heap-walking library
    libraries/ghc-prim/ghc-prim.cabal:       Core library
    libraries/haskeline/haskeline.cabal:     Dependency of ``ghci`` executable
    libraries/hpc/hpc.cabal:                 Dependency of ``hpc`` executable
    libraries/integer-gmp/integer-gmp.cabal: Core library
    libraries/mtl/mtl.cabal:                 Dependency of ``Cabal`` library
    libraries/parsec/parsec.cabal:           Dependency of ``Cabal`` library
    libraries/pretty/pretty.cabal:           Dependency of ``ghc`` library
    libraries/process/process.cabal:         Dependency of ``ghc`` library
    libraries/stm/stm.cabal:                 Dependency of ``haskeline`` library
    libraries/template-haskell/template-haskell.cabal: Core library
    libraries/terminfo/terminfo.cabal:       Dependency of ``haskeline`` library
    libraries/text/text.cabal:               Dependency of ``Cabal`` library
    libraries/time/time.cabal:               Dependency of ``ghc`` library
    libraries/transformers/transformers.cabal: Dependency of ``ghc`` library
    libraries/unix/unix.cabal:               Dependency of ``ghc`` library
    libraries/Win32/Win32.cabal:             Dependency of ``ghc`` library
    libraries/xhtml/xhtml.cabal:             Dependency of ``haddock`` executable
    libraries/os-string/os-string.cabal:     Dependency of ``filepath`` library
